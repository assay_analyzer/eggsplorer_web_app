#
# Whitefly Assay Analyzer Web App Version
# Authored by: DJAR
# Wageningen University and Research
# Greenhouse Technology Business Unit
# 2022/08/13
#


import os
import cv2
import torch
import torch.backends.cudnn as cudnn
from models.common import DetectMultiBackend
from utils.general import (LOGGER, check_file, check_img_size, check_imshow, check_requirements, colorstr,
                           increment_path, non_max_suppression, print_args, scale_coords, strip_optimizer, xyxy2xywh)
from utils.torch_utils import select_device
from utils.augmentations import letterbox
from utils.testing_tools import *

import numpy as np
import shutil
import math
import time
import pandas as pd
from datetime import datetime
import json

from flask import send_from_directory
from flask import jsonify
from flask import Flask, render_template, request, Response, send_file, redirect
from flask_dropzone import Dropzone
from zipfile import ZipFile
basedir = r"D:/Dropbox/Dropbox/CLOUD_SW/eggsplorer_web_app"
os.chdir(r"D:/Dropbox/Dropbox/CLOUD_SW/eggsplorer_web_app")

import dtools.image_tiler as tile
import time



#
# Algorithm constants
#
project_name = "whitefly_assay"
weights_filename = "weights/{}/batch3.pt".format(project_name)
crop_size = 1600
image_size_network = (640, 640)
image_size_real = 1600
conf_thres = 0.2
iou_thres = 0.2
L_filter = 0
W_filter = 0
classes = 0
padding = 200


save_image = True       # save the image file
save_txt = False        # save txt file
save_conf = False       # save confidence
save_zip = True

xlsx_output_dir = "output_xlsx"
try:
    os.mkdir(xlsx_output_dir)
except:
    pass

zip_output_dir = "output_zip"
try:
    os.mkdir(zip_output_dir)
except:
    pass





#
# Define a tiler object
#
PERFORM_NMS = 1
if PERFORM_NMS == 1:
    tiler = tile.tiler(crop_size, padding)
else:
    tiler = tile.tiler(crop_size, 0)

#
# normalization gain whwh
#
gn = torch.tensor([image_size_real, image_size_real, image_size_real, image_size_real])  

#
# Load model
#
half = True
device = select_device()
model = DetectMultiBackend(weights_filename, device=device)
model.model.half()

#
# Initialize Flask app
#
from logging import FileHandler,WARNING
app = Flask(__name__)
app.config.update(
    UPLOADED_PATH= os.path.join(basedir,'uploads'),
    DROPZONE_MAX_FILE_SIZE = 1024*50,
    DROPZONE_TIMEOUT = 60*60*1000)
file_handler = FileHandler('errorlog.txt')
file_handler.setLevel(WARNING)
dropzone = Dropzone(app)



image = ""
xlsx_filename = ""
zip_filename = ""
filenames = []


def process_image(fname, CONF_THRESHOLD = 0.5, PERFORM_NMS=0):
    columns = ["Index", "DateTime", "Filename" ,"Count", "ElapsedTime", "Threshold"]
    xlsx_df = pd.DataFrame(columns=columns)

    start = time.time()
    fname = str(fname)
    if fname != "":
        all_coords = []
        all_labels = []
        all_confs = []
        image = cv2.imread(fname)

        #
        # Make the image suitable for tiling
        #
        cropped_image = tiler.remove_borders(image)
        
        #
        # Tile the image
        #
        tiled_images = tiler.tile_image(cropped_image)
        for item in tiled_images:
            y = item[0]
            x = item[1]
            tiled_image = item[2]

            #
            # Read, resize and condition image
            #
            im0 = tiled_image.copy()


            im = letterbox(im0, image_size_network)[0]
            im = im.transpose((2, 0, 1))[::-1]  # HWC to CHW, BGR to RGB
            im = np.ascontiguousarray(im)
            
            #
            # Prepare image for inference
            #
            im = torch.from_numpy(im).to(device)
            im = im.half() if half else im.float()  # uint8 to fp16/32
            im /= 255  # 0 - 255 to 0.0 - 1.0
            if len(im.shape) == 3:
                im = im[None]  
            
            #
            # Inference
            #
            pred = model(im)
            pred = non_max_suppression(pred, conf_thres, iou_thres, classes, max_det=10000)
            
        
            #
            # Process predictions
            #
            n = 0
            for i, det in enumerate(pred):  
                if len(det):
                    #
                    # Rescale boxes from img_size to im0 size
                    #
                    det[:, :4] = scale_coords(im.shape[2:], 
                                            det[:, :4], 
                                            im0.shape).round()
                    
                    #
                    # Get number of detections
                    #
                    for c in det[:, -1].unique():
                        n = (det[:, -1] == c).sum()  # detections per class
                        
                    #
                    # Write results
                    #
                    for *xyxy, conf, cl in reversed(det):
                        if conf >= CONF_THRESHOLD:
                            coords = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()
                            coords = yolobbox2bbox(*coords, image_size_real)
                            coords = [int(c) for c in coords]
                
                            #
                            # Retranslate coordinates
                            # 
                            new_coords = [0] * 4
                            new_coords[0] = coords[0] + (y * image_size_real)
                            new_coords[1] = coords[1] + (x * image_size_real)
                            new_coords[2] = coords[2] + (y * image_size_real)
                            new_coords[3] = coords[3] + (x * image_size_real)

                            L = abs(new_coords[0]  - new_coords[2])
                            W = abs(new_coords[1]  - new_coords[3])


                            if L >= L_filter and W >= W_filter:
                                all_coords.append(new_coords)
                                all_labels.append(int(cl))
                                all_confs.append(float(conf))

        
        
        #
        # Perform merging of detections from tiling
        #
        if PERFORM_NMS == 1:
            final_coords, final_confs = tile.non_max_suppression(all_coords, all_confs, 0.3)
        else:
            final_coords = all_coords
            final_confs = all_confs


        #
        # Mark the image
        #
        output_image = cropped_image.copy()
        for f, conf in zip(final_coords, final_confs):
            start_point = (int(f[0]), int(f[1]))
            end_point = (int(f[2]), int(f[3]))   
            txt_point = [f[0]-15, f[1]-15]
            output_image = cv2.rectangle(cropped_image, start_point, end_point, (0,0,255), thickness=3)
            output_image = cv2.putText(cropped_image, 
                                str(round(float(conf), 2)), 
                                txt_point, 
                                cv2.FONT_HERSHEY_SIMPLEX, 
                                1, 
                                (0,0,255), 
                                2, 
                                cv2.LINE_AA)   
        
        end = time.time()
        elapsed_time = round((end-start), 3)
        return (output_image, len(final_coords), elapsed_time)




                

@app.route('/download_images', methods=['GET', 'POST'])
def download_images():   
    global zip_filename
    bname = os.path.basename(zip_filename)
    return send_file(
        zip_filename,
        mimetype='text/csv',
        download_name=bname,
        as_attachment=True
    )
             
@app.route('/download_xlsx', methods=['GET', 'POST'])
def download_xlsx():   
    global xlsx_filename
    bname = os.path.basename(xlsx_filename)
    return send_file(
        xlsx_filename,
        mimetype='text/csv',
        download_name=bname,
        as_attachment=True
    )
        
@app.route('/clear_images', methods=['GET', 'POST'])
def clear_images():   
    global filenames
    filenames = []
    print("Cleared images!")
    return True
    # return jsonify(filenames=filenames)

@app.route('/process_images', methods=['GET', 'POST'])
def process_images():   
    global filenames
    global xlsx_filename
    global zip_filename

    if request.method == 'POST':
        print(filenames)
        #
        # Create blank xlsx file
        #
        columns = ["Index", "DateTime", "Filename" ,"Count", "ElapsedTime", "Threshold"]
        xlsx_df = pd.DataFrame(columns=columns)

        #
        # Use datetime as filename
        #
        now = datetime.now()
        dt_string = now.strftime("%Y/%m/%d %H:%M:%S")
        dt_string2 = now.strftime("%Y_%m_%d %H_%M_%S")


        new_filenames = []
        counts = []
        elapsed_times = []
        try:
            data = request.get_json()
            CONF_THRESHOLD = float(data["threshold"])
        except Exception as e:
            print(e)


        print("")
        print("[SYSTEM] Now processing {} images using threshold={}...".format(len(filenames), CONF_THRESHOLD))

        

        #
        # Process each image
        #
        for f_index, fname in enumerate(filenames):
            bname = os.path.splitext(os.path.basename(fname))[0] + "_{}".format(dt_string2) + ".jpg"
            new_filename = "static/{}".format(bname)

            #
            # Process image
            #
            output_image, eggs_count, elapsed_time = process_image(fname, CONF_THRESHOLD=CONF_THRESHOLD, PERFORM_NMS=PERFORM_NMS)

            #
            # Append output to xlsx file
            #
            xlsx_df = xlsx_df.append({"Index": f_index+1, "DateTime": dt_string, "Filename": os.path.basename(new_filename),
                                        "Count": eggs_count, "ElapsedTime": elapsed_time, "Threshold": CONF_THRESHOLD}, ignore_index=True)
                        
            #
            # Resize image to prevent lags in website
            #
            new_width = int(output_image.shape[1]*0.9)
            new_height = int(output_image.shape[0]*0.9)
            img_half = cv2.resize(output_image, (new_width, new_height))

            #
            # Save output image
            #
            cv2.imwrite(new_filename, img_half)


            print("[SYSTEM] {}/{} {} Count: {} Time elapsed: {}".format(f_index+1, 
                                                                        len(filenames), 
                                                                        fname, 
                                                                        eggs_count, 
                                                                        elapsed_time))
            new_filenames.append(new_filename)
            counts.append(eggs_count)
            elapsed_times.append(elapsed_time)
        print("")


    
    #
    # Store images into a zip file
    #
    if save_zip == True:
        zip_filename = zip_output_dir + "/" + dt_string2 + ".zip"
        with ZipFile(zip_filename, 'w') as zipObj:
            for fname in new_filenames:
                zipObj.write(fname)
        zipObj.close()
        
        

    #
    # Save xlsx
    #
    xlsx_filename = xlsx_output_dir + "/" + dt_string2 + ".xlsx"
    xlsx_df.to_excel(xlsx_filename, index=False)

    filenames = []

    return jsonify(new_filenames=new_filenames, 
                    counts=counts, 
                    elapsed_times=elapsed_times)
            

    
    

@app.route('/',methods=['POST','GET'])
def upload():
    global filenames
    if request.method == 'POST':
        try:
            f = request.files.get('file')
            print(f)
            new_filename = os.path.join(app.config['UPLOADED_PATH'],f.filename)
            f.save(new_filename)
            print(new_filename)
            filenames.append(new_filename)
        except Exception as e:
            print(e)
    return render_template('index.html')

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=10000, debug=False)
